package com.rave.shibe.view.shibelist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.rave.shibe.databinding.FragmentShibeListBinding
import com.rave.shibe.model.ShibeRepo
import com.rave.shibe.viewmodel.ShibeViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ShibeListFragment : Fragment() {

    private var _binding: FragmentShibeListBinding? = null
    private val binding get() = _binding!!
    @Inject lateinit var repo: ShibeRepo
    private val shibeViewModel by viewModels<ShibeViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShibeListBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shibeViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.run {
                progress.isVisible = state.isLoading
                rvShibeList.adapter = ShibeAdapter(state.shibes, repo)
            }
        }
//        binding.
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}