package com.rave.shibe.model

import android.content.Context
import com.rave.shibe.model.local.ShibeDatabase
import com.rave.shibe.model.local.entity.Shibe
import com.rave.shibe.model.remote.ShibeService
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

//private const val TAG = "ShibeRepo"
@Singleton
class ShibeRepo @Inject constructor(
     val shibeService: ShibeService, @ApplicationContext context: Context
) {

    //    private val shibeService = ShibeService.getInstance()
    val shibeDao = ShibeDatabase.getInstance(context).shibeDao()
    suspend fun getShibes() = withContext(Dispatchers.IO) {
        val cachedShibes: List<Shibe> = shibeDao.getAll()

        return@withContext cachedShibes.ifEmpty {
            val shibeUrls: List<String> = shibeService.getShibes()
            val shibes: List<Shibe> = shibeUrls.map { Shibe(url = it) }
            shibeDao.insert(shibes)
            return@ifEmpty shibes
        }
    }
}