package com.rave.shibe

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ShibeApp : Application() {
}