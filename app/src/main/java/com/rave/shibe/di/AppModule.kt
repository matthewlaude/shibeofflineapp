package com.rave.shibe.di

import com.rave.shibe.model.remote.ShibeService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun providesShibeService() : ShibeService = ShibeService.getInstance()
}